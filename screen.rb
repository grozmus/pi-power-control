require 'curses'
include Curses

class Screen

  def initialize
    noecho # do not show typed keys
    init_screen
    stdscr.keypad(true)
    crmode
  end

  def display(state)
    setpos(1,1)
    state.each_index do |i|
      addstr(state_string(state, i))
      addstr("  ")
    end
    setpos(3,1)
    addstr("Q: Quit")
    refresh
  end

  def wait_on_key
    getch
  end

  def print
    win = Curses::Window.new(5, 20, 0, 0)
    win.box("|", "-")
    win.setpos(2, 3)
    win.addstr("TEST")
    win.refresh
    win.getch
    win.close
  end

  def close
    close_screen
  end

  private
  def state_string(state, pos)
    "K#{pos+1}:#{state[pos] ? 'off' : 'on'}"
  end
end