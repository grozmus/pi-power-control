require 'curses'

Curses.noecho # do not show typed keys
Curses.init_screen
Curses.stdscr.keypad(true)

begin
  Curses.crmode

  loop do
    ch = Curses.getch
    case ch
      when 'q'
        break;
      when '1'
        Curses.addstr("x")
        Curses.refresh
    end
  end

ensure
  Curses.close_screen
end
