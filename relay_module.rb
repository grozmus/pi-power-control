require 'rpi_gpio'

include RPi

class RelayModule
  attr_reader :state

  PINS = [7, 0, 2, 3]
  PINS_BOARD = [7, 11, 13, 15]

  def initialize
    @state = [true, true, true, true]
    init_board
    read_all
  end

  def toggle(relay)
    @state[relay] = !@state[relay]
    if @state[relay]
      GPIO.set_high PINS_BOARD[relay]
    else
      GPIO.set_low PINS_BOARD[relay]
    end
  end

  private
  def init_board
    GPIO.set_numbering :board
    setup_pins
  end

  def setup_pins
    PINS_BOARD.each do |pin|
      GPIO.setup pin, :as => :output
    end
  end

  def read_all
    @state.each_index do |i|
      @state[i] = GPIO.high?(PINS_BOARD[i])
    end
  end
end