require 'curses'
require './relay_module'
require './screen'

include Curses

class Controller

  def initialize
    @relay = RelayModule.new
    @screen = Screen.new
    @screen.display(@relay.state)
  end

  def listen
    begin
      loop do
        ch = getch
        case ch
          when 'q'
            break
          when '1'
            change_state(0)
          when '2'
            change_state(1)
          when '3'
            change_state(2)
          when '4'
            change_state(3)
        end
      end
    ensure
      @screen.close
    end
  end

  private
  def change_state(num)
    @relay.toggle(num)
    @screen.display(@relay.state)
  end
end

controller = Controller.new
controller.listen